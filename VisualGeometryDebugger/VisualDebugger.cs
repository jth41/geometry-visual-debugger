﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.DebuggerVisualizers;



[assembly: System.Diagnostics.DebuggerVisualizer(
typeof(VisualDebugger.ImageDebugger),
typeof(VisualizerObjectSource),
Target = typeof(System.Drawing.Image),
Description = "Image Visualizer")]

namespace VisualDebugger
{

    public class ImageDebugger : Microsoft.VisualStudio.DebuggerVisualizers.DialogDebuggerVisualizer
    {

        protected override void Show(Microsoft.VisualStudio.DebuggerVisualizers.IDialogVisualizerService windowService, Microsoft.VisualStudio.DebuggerVisualizers.IVisualizerObjectProvider objectProvider)
        {
            System.Drawing.Image image = (Image)objectProvider.GetObject();

            Form frm = new Form();
            frm.Text = "Custom Visualizer - " + image.HorizontalResolution.ToString() + " " + image.VerticalResolution.ToString();
            frm.Width = image.Width;
            frm.Height = image.Height;

            PictureBox pic = new PictureBox();
            pic.Image = image;
            pic.SizeMode = PictureBoxSizeMode.AutoSize;

            frm.Controls.Add(pic);
            frm.ShowDialog();

        }
    }
}